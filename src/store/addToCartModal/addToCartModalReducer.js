import initialStore from "../initialStore";
import {MODAL_ADD_OPEN, MODAL_ADD_CLOSE} from "./addToCartModalActions";

export default function addToCartModalReducer(isAddToCartModalOpen = initialStore.isAddToCartModalOpen, action) {
    switch (action.type) {
        case MODAL_ADD_OPEN:
            return action.payload;
        case MODAL_ADD_CLOSE:
            return action.payload;

        default:
            return isAddToCartModalOpen;
    }
}