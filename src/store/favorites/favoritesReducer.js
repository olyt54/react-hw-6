import initialStore from "../initialStore";
import {UPDATE_FAVORITES} from "./favoritesActionTypes";



export default function favoritesReducer (favoritesFromStore=initialStore.goods, action) {
    switch (action.type) {
        case UPDATE_FAVORITES:
            return action.payload
        default:
            return favoritesFromStore;
    }
}