import store from "../configStore";
import {RESET_CART, UPDATE_CART} from "./cartActionTypes";


export function updateCart() {
    return (dispatch) => {
        const goods = store.getState().goods;
        const cartCodes = JSON.parse(localStorage.getItem("cart"));
        const newCart = goods.filter(item => cartCodes.includes(item.vendorCode));

        dispatch({
            type: UPDATE_CART,
            payload: newCart
        })
    }
}

export const resetCart = () => (dispatch) => {
    localStorage.setItem("cart", JSON.stringify([]));

    dispatch({
        type: RESET_CART,
        payload: []
    })
}