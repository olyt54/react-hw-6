import {SET_CHECKOUT_FORM} from "./actionTypes";

export const setForm = (values) => (dispatch) => {
    dispatch({
        type: SET_CHECKOUT_FORM,
        payload: values
    })

}