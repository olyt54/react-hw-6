import initialStore from "../initialStore";
import {SET_CHECKOUT_FORM} from "./actionTypes";
import CheckoutForm from "../../components/CheckoutForm/CheckoutForm";

export const checkoutFormReducer = (checkoutForm=initialStore.checkoutForm, action) => {
    switch (action.type) {
        case SET_CHECKOUT_FORM:
            return action.payload
        default:
            return CheckoutForm;
    }
}