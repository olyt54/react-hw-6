export const MODAL_REMOVE_OPEN = "MODAL_REMOVE_OPEN";
export const MODAL_REMOVE_CLOSE = "MODAL_REMOVE_CLOSE";

export function openRemoveModal(vendorCode) {
    return (dispatch) => {
        dispatch({
            type: MODAL_REMOVE_OPEN,
            payload: vendorCode
        })
    }
}

export function closeRemoveModal() {
    return (dispatch) => {
        dispatch({
            type: MODAL_REMOVE_CLOSE,
            payload: ""
        })
    }
}