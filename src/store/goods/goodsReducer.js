import initialStore from "../initialStore";
import {GOODS_LOAD} from "./goodsActions";


export default function goodsReducer (goodsFromStore=initialStore.goods, action) {
    switch (action.type) {
        case GOODS_LOAD:
            return action.payload
        default:
            return goodsFromStore;
    }
}