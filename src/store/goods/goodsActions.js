import axios from "axios";

export const GOODS_LOAD = "GOODS_LOAD";

export function goodsLoad() {
    return (dispatch) => {
        axios("/goods.json")
            .then(res => {
                dispatch({
                    type: GOODS_LOAD,
                    payload: res.data
                })
            })

    }
}