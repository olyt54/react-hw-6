import React from 'react';
import {Switch, Route} from "react-router-dom";
import Home from "../pages/Home";
import Favorites from "../pages/Favorites";
import Cart from "../pages/cart/Cart";


const AppRoutes = ({goods}) => {
    return (
        <Switch>
            <Route exact path="/" render={(routeProps) => <Home goods={goods} {...routeProps}/>} />
            <Route exact path="/cart" render={(routeProps) => <Cart goods={goods} isInCart={true} {...routeProps}/>}/>
            <Route exact path="/favorites" render={(routeProps) => <Favorites goods={goods} isInFavorites={true} {...routeProps}/>}/>
        </Switch>
    );
};

export default AppRoutes;