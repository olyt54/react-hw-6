import React from 'react';
import "./EmptyContent.scss";

const EmptyContent = () => {
    return (
        <div className="empty">
            <p>No items has been added</p>
        </div>
    );
};

export default EmptyContent;