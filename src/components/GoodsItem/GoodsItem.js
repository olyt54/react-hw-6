import React, {useState} from 'react';
import Button from "../Button/Button";
import StarIcon from "../StarIcon/StarIcon";
import propTypes from "prop-types"
import "./GoodsItem.scss"
import Modal from "../Modal/Modal";
import CloseIcon from "../CloseIcon/CloseIcon";
import {connect} from "react-redux"
import {closeAddModal, openAddModal} from "../../store/addToCartModal/addToCartModalActions";
import {closeRemoveModal, openRemoveModal} from "../../store/removeFromCartModal/removeFromCartModalActions";

const GoodsItem = (props) => {

    const checkIsFavorite = () => {
        const favorite = JSON.parse(localStorage.getItem("favorites"));
        return !!favorite.find(i => i === props.vendorCode);
    }
    const [isFavorite, setIsFavorite] = useState(checkIsFavorite());

    const chooseModal = () => {
        let modal;

        if (props.isAddToCartModalOpen === props.vendorCode) {
            modal = <Modal
                header={"Adding to cart"}
                text={"You're about to add this item to cart, are you sure?"}
                closeButton={true}
                handleCloseOuter={closeModalOuter}
                handleCloseButton={props.closeAddModal}
                actions={{
                    firstBtn: <Button text={"Ok"} bgColor={"rgba(0, 0, 0, .5)"} handleBtn={addToCart}/>,
                    secondBtn: <Button text={"Cancel"} bgColor={"rgba(0, 0, 0, .5)"} handleBtn={props.closeAddModal}/>
                }}
            />
        }

        if (props.isRemoveFromCartModalOpen === props.vendorCode) {
            modal = <Modal
                header={"Removing from cart"}
                text={"You're about to remove this item from cart, are you sure?"}
                closeButton={true}
                handleCloseOuter={closeModalOuter}
                handleCloseButton={props.closeRemoveModal}
                actions={{
                    firstBtn: <Button text={"Ok"} bgColor={"rgba(0, 0, 0, .5)"} handleBtn={removeFromCart}/>,
                    secondBtn: <Button text={"Cancel"} bgColor={"rgba(0, 0, 0, .5)"}
                                       handleBtn={props.closeRemoveModal}/>
                }}
            />
        }

        return modal;
    }

    const closeModalOuter = (event) => {
        if (event.target === event.currentTarget) {
            if (props.isAddToCartModalOpen) {
                props.closeAddModal();
            } else if (props.isRemoveFromCartModalOpen) {
                props.closeRemoveModal();
            }
        }
    }

    const checkIfAddedToCart = (cartFromStorage) => {
        return !!cartFromStorage.find(i => i === props.vendorCode);
    }

    const {updateFavorites, updateCart, location} = props;

    const addToCart = () => {
        const localCart = JSON.parse(localStorage.getItem("cart"));

        if (!checkIfAddedToCart(localCart)) {
            localCart.push(props.vendorCode)
            localStorage.setItem("cart", JSON.stringify(localCart));
        }

        props.closeAddModal();
    }

    const removeFromCart = () => {
        const cart = JSON.parse(localStorage.getItem("cart")),
            newCart = cart.filter(i => i !== props.vendorCode);
        localStorage.setItem("cart", JSON.stringify(newCart))

        props.closeRemoveModal();

        if (location.pathname === "/cart") {
            updateCart(props.vendorCode)
        }
    }

    const addToFavorites = () => {
        setIsFavorite(!isFavorite);

        const favorites = JSON.parse(localStorage.getItem("favorites"));
        favorites.push(props.vendorCode)
        localStorage.setItem("favorites", JSON.stringify(favorites));
    }

    const removeFromFavorites = () => {
        setIsFavorite(!isFavorite);

        const favorites = JSON.parse(localStorage.getItem("favorites")),
            newFavorites = favorites.filter(i => i !== props.vendorCode);
        localStorage.setItem("favorites", JSON.stringify(newFavorites))

        if (location.pathname === "/favorites") {
            updateFavorites(props.vendorCode);
        }
    }

    const toggleFavorites = () => {
        if (!isFavorite) {
            addToFavorites();
        } else {
            removeFromFavorites()
        }
    }

    const chooseFavoritesIcon = () => {
        if (isFavorite) {
            return <StarIcon color="#ffc107"/>
        }

        return <StarIcon color={"gray"}/>
    }

    const {title, price, url, vendorCode, color, isInCart} = props;

    return (
        <div className="goods-item">
            {isInCart && <Button text={<CloseIcon color="#606060" className="goods-item__close-svg"/>}
                                 bgColor="transparent" className="goods-item__close-btn"
                                 handleBtn={() => props.openRemoveModal(vendorCode)}
            />}
            <img className="goods-item__img" src={url} alt="Brick"/>
            <div className="goods-item__desc">
                <div className="goods-item__head-container">
                    <h3 className="goods-item__head-text">{title}</h3>
                    <Button text={chooseFavoritesIcon()}
                            bgColor={"rgba(0, 0, 0, 0)"}
                            handleBtn={() => toggleFavorites()}
                    />
                </div>
                <p className="goods-item__text">{`article: ${vendorCode}`}</p>
                <p className="goods-item__text">{`Color: ${color}`}</p>
                <div className="goods-item__price-container">
                    <p className="goods-item__text">{`$${price}`}</p>
                    <Button text={"Add to cart"}
                            bgColor={"#606060"}
                            handleBtn={() => props.openAddModal(vendorCode)}
                    />
                </div>
            </div>
            {
                chooseModal()
            }
        </div>
    );
}

GoodsItem.propTypes = {
    title: propTypes.string,
    price: propTypes.number,
    url: propTypes.string,
    vendorCode: propTypes.number,
    color: propTypes.string,
    handleAddToCart: propTypes.func
}

const mapStoreToProps = (store) => ({
    isAddToCartModalOpen: store.isAddToCartModalOpen,
    isRemoveFromCartModalOpen: store.isRemoveFromCartModalOpen
})

const mapDispatchToProps = (dispatch) => ({
    openAddModal: (vendorCode) => dispatch(openAddModal(vendorCode)),
    closeAddModal: () => dispatch(closeAddModal()),
    openRemoveModal: (vendorCode) => dispatch(openRemoveModal(vendorCode)),
    closeRemoveModal: () => dispatch(closeRemoveModal())
})

export default connect(mapStoreToProps, mapDispatchToProps)(GoodsItem);