import React from "react";
import {render} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Modal from "./Modal";


const closeOuter = jest.fn(),
    {click} = userEvent,
    modal = <Modal handleCloseOuter={closeOuter}/>;

describe("Modal unit test", () => {
    test("smoke modal test", () => {
        render(modal);
    });

    test("close on click empty space", () => {
        const {getByTestId} = render(modal),
            bg = getByTestId("modal-bg");

        click(bg);
        expect(closeOuter).toBeCalled();
    })

});