import React from 'react';
import Button from "../Button/Button";
import propTypes from "prop-types"
import "./modal.scss"
import CloseIcon from "../CloseIcon/CloseIcon";

const Modal = ({closeButton, handleCloseButton, handleCloseOuter, header, text, actions}) => {
    let closeBtn;
    if (closeButton) {
        closeBtn = <Button text={<CloseIcon color="white"/>} bgColor="transparent" handleBtn={handleCloseButton}/>
    }

    return (
        <div data-testid="modal-bg" className="modal-bg" onClick={(event) => handleCloseOuter(event)}>
            <div className="modal">
                <div className="modal-head">
                    <h3 className="modal-head-text">{header}</h3>
                    {closeBtn}
                </div>
                <div className="modal-body">
                    <p className="modal-body-text">{text}</p>
                    <div className="modal-actions">
                        {actions && actions.firstBtn}
                        {actions && actions.secondBtn}
                    </div>
                </div>
            </div>
        </div>
        );
}

Modal.propTypes = {
    closeButton: propTypes.bool,
    handleClose: propTypes.func,
    header: propTypes.string,
    text: propTypes.string,
    actions: propTypes.object
}

export default Modal;

