import React from "react";
import {render} from "@testing-library/react";
import Button from "./Button";
import userEvent from "@testing-library/user-event";

const handler = jest.fn(),
    submitHandler = jest.fn(),
    {click} = userEvent,
    className = "btn-test",
    btn = <Button text="text" handleBtn={handler} className={className}/>,
    submitBtn = <Button text="text" handleBtn={submitHandler} type="submit"/>;

describe("button unit tests", () => {
    test("smoke test button", () => {
        render(btn)
    });

    test("is button handler called", () => {
        const {getByText} = render(btn);

        click(getByText("text"));
        expect(handler).toBeCalled();

    });

    test("is button handler not called with type", () => {
        const {getByText} = render(submitBtn);

        click(getByText("text"));
        expect(submitHandler).not.toBeCalled();
    });

    test("additional class test", () => {
        const {getByText} = render(btn),
            btnNode = getByText("text");

        expect(btnNode).toHaveClass(className);
    });
})