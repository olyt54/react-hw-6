import React from 'react';
import "./CheckoutForm.scss";
import {Form, Field, withFormik} from "formik";
import * as yup from "yup";
import Button from "../Button/Button";
import {useDispatch} from "react-redux";
import {setForm} from "../../store/checkoutForm/actions";

const formSchema = yup.object().shape({
    name: yup
        .string()
        .required("This is required field"),
    lastName: yup
        .string()
        .required("This is required field"),
    age: yup
        .number("Must be a number")
        .required("This is required field")
        .positive("Must be bigger than 0")
        .integer("Must be an integer"),
    address: yup
        .string()
        .required("This is required field"),
    phone: yup
        .number("Must be a number")
        .required("This is required field")
        .min(10, "Your phone number to short")
})

const submitForm = (values, formProps) => {
    const {cart, updateCart} = formProps.props;
    console.log("Data from form -", values);
    console.log("Ordered next items -", cart);

    localStorage.setItem("cart", JSON.stringify([]));
    updateCart();
}

const CheckoutForm = (props) => {
    const dispatch = useDispatch();
    const {values, errors} = props;

    dispatch(setForm(values));

    return (
        <Form className="checkout-form">
            <h3 className="checkout-form__head">Checkout proceed</h3>
            <label className="checkout-form__label">
                {errors.name && <span className="checkout-form__error">{errors.name}</span>}
                <Field component="input"
                       type="text"
                       name="name"
                       placeholder="Enter your name*"
                       className="checkout-form__input"
                />
            </label>
            <label className="checkout-form__label">
                {errors.lastName && <span className="checkout-form__error">{errors.lastName}</span>}
                <Field component="input"
                       type="text"
                       name="lastName"
                       placeholder="Enter your last name*"
                       className="checkout-form__input"
                />
            </label>
            <label className="checkout-form__label">
                {errors.age && <span className="checkout-form__error">{errors.age}</span>}
                <Field component="input"
                       type="text"
                       name="age"
                       placeholder="Enter your age*"
                       className="checkout-form__input"
                />
            </label>
            <label className="checkout-form__label">
                {errors.address && <span className="checkout-form__error">{errors.address}</span>}
                <Field component="input"
                       type="text"
                       name="address"
                       placeholder="Enter your shipping address*"
                       className="checkout-form__input"
                />
            </label>
            <label className="checkout-form__label">
                {errors.phone && <span className="checkout-form__error">{errors.phone}</span>}
                <Field component="input"
                       type="tel"
                       name="phone"
                       placeholder="Enter your phone number*"
                       className="checkout-form__input"
                />
            </label>
            <Button type="submit"
                    text="Checkout"
                    bgColor="#606060"
            />

        </Form>
    );
};

export default withFormik({
    mapPropsToValues: () => ({
        name: "",
        lastName: "",
        age: "",
        address: "",
        phone: ""
    }),
    validationSchema: formSchema,
    validateOnChange: false,
    validateOnBlur: false,
    handleSubmit: submitForm
})(CheckoutForm);