import React, {useEffect} from 'react';
import GoodsList from "../../components/GoodsList/GoodsList";
import {connect} from "react-redux";
import {updateCart} from "../../store/cart/cartActions";
import CheckoutForm from "../../components/CheckoutForm/CheckoutForm";
import "./Cart.scss"
import EmptyContent from "../../components/EmptyContent/EmptyContent";

const Cart = (props) => {
    const {goods, cart, updateCart, isInCart, location} = props;


    useEffect(() => {
        updateCart();
    }, [goods, updateCart]);

    const removeFromCart = (vendorCode) => {
        const newCart = cart.filter(i => i.vendorCode !== vendorCode);

        updateCart(newCart);
    }

    return (
        <div className="cart">
            <div className="form-container">
                <CheckoutForm cart={cart} updateCart={updateCart}/>
            </div>
            <div className="cart-container">
                {
                    cart.length >= 1
                        ? <GoodsList goods={cart}
                                     isInCart={isInCart}
                                     updateCart={removeFromCart}
                                     location={location}
                        />
                        : <EmptyContent/>
                }
            </div>
        </div>
    );
};

const mapStoreToProps = (store) => ({
    cart: store.cart
})

const mapDispatchToProps = (dispatch) => ({
    updateCart: (cart) => dispatch(updateCart(cart))
})

export default connect(mapStoreToProps, mapDispatchToProps)(Cart)

