import React, {useEffect} from 'react';
import GoodsList from "../components/GoodsList/GoodsList";
import {connect} from "react-redux";
import {updateFavorites} from "../store/favorites/favoritesActions";
import EmptyContent from "../components/EmptyContent/EmptyContent";


const Favorites = (props) => {
    const {goods, favorites, updateFavorites, isInFavorites} = props;

    useEffect(() => {
        updateFavorites()
    }, [goods, updateFavorites]);

    const removeFromFavorites = (vendorCode) => {
        const newFavorites = favorites.filter(i => i.vendorCode !== vendorCode);

        updateFavorites(newFavorites)
    }
    favorites ? console.log(true) : console.log(false)
    return (
        <div className="goods-container">
            {
                favorites.length >= 1
                    ? <GoodsList goods={favorites}
                                 updateFavorites={removeFromFavorites}
                                 location={props.location}
                                 isInFavorites={isInFavorites}/>
                    : <EmptyContent/>
            }
        </div>
    );
};

const mapStoreToProps = (store) => ({
    favorites: store.favorites
})

const mapDispatchToProps = (dispatch) => ({
    updateFavorites: (favorites) => dispatch(updateFavorites(favorites))
})

export default connect(mapStoreToProps, mapDispatchToProps)(Favorites);