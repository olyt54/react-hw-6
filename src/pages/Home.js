import React from 'react';
import GoodsList from "../components/GoodsList/GoodsList";

const Home = (props) => {
    const {goods, location} = props;

    return (
        <div className="goods-container">
            <GoodsList goods={goods} location={location}/>
        </div>
    );
};

export default Home

